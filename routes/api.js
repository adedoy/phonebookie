var data = {
  "contacts": [
    { name: "Richard", email:'leapzig@gmail.com', number:'0917-1642134'},
    { name: "Archird", email:'leapzig@gmail.com', number:'0917-1642134'},
    { name: "Dhracir", email:'leapzig@gmail.com', number:'0917-1644134'},
    { name: "Irdhrac", email:'leapzig@gmail.com', number:'0917-1645134'},
    { name: "Rachird", email:'leapzig@gmail.com', number:'0917-1646134'},
    { name: "Chirdra", email:'leapzig@gmail.com', number:'0917-1648134'}
  ]
};

exports.contacts = function (req, res) {
	var contacts = [];
	data.contacts.forEach(function (contact, i) {
		contacts.push({
			id: i,
			name: contact.name,
			number: contact.number
		});
	});
	res.json({
		contacts: contacts
	});
};

exports.contact = function (req, res) {
	var id = req.params.id;
	if (id >= 0 && id < data.contacts.length) {
		res.json({
			contact: data.contacts[id]
		});
	} else {
		res.json(false);
	}
};

exports.addContact = function (req, res) {
	data.contacts.push(req.body);
	res.json(req.body);
};

exports.editContact = function (req, res) {
	var id = req.params.id;
	
	if (id >= 0 && id < data.contacts.length) {
		data.contacts[id] = req.body;
		res.json(true);
	} else {
		res.json(false);
	}
};

exports.deleteContact = function (req, res) {
	var id = req.params.id;
	if (id >= 0 && id < data.contacts.length) {
		data.contacts.splice(id, 1);
		res.json(true);
	} else {
		res.json(false);
	}
};