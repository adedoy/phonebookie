var	gulp  = require('gulp'),
	  concat = require("gulp-concat"),
  	minifyCSS = require('gulp-minify-css');
  	
gulp.task('css', function(){
		gulp.src('/public/css/*.css')
	 .pipe(concat('site.css'))  
	 .pipe(minifyCSS())
	 .pipe(gulp.dest('public/css'))
});
gulp.task('minify-js', function () {
    gulp.src('/public/js/*.js') // path to your files
	 .pipe(concat('site.js'))  
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
});

gulp.task('default', function() {
	gulp.watch('/public/css/*.js', function(){
		gulp.run('css');
	});
	gulp.watch('/public/js/*.js', function(){
		gulp.run('minify-js');
	});
});
