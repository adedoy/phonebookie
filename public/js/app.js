(function() {
  var phoneBookApp = angular.module('phoneBookApp', ['ui.router']);

  phoneBookApp
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider.state('/add', {
      url: '/add/contact',
      templateUrl: '/add',
      controller: 'AddContactCtrl'
    }).state('/view', {
      url: '/view/:id',
      templateUrl: '/view',
      controller: 'ViewContactCtrl'
    }).state('/edit', {
      url: '/edit/:id',
      templateUrl: '/edit',
      controller: 'EditContactCtrl'
    }).state('/delete', {
      url: '/delete/:id',
      templateUrl: '/delete',
      controller: 'DeleteContactCtrl'
    });
  })
  .controller('IndexCtrl', function($scope, $http, $location, $timeout) {
    $scope.contacts = {};
    $http.get('/api/contacts')
      .success(function(data) {
        $scope.contacts = data.contacts;
      });
    $scope.$on('updateContact', function(e, a) {
      $scope.xmessage = a.message;
      $scope.xmclass = a.xmclass;
      // Updates the view
      $timeout((function() {
        $http.get('/api/contacts')
        .success(function(data) {
          $scope.contacts = data.contacts;
        });
      }), 0);
      $timeout((function() {
        $scope.xmessage = null;
        $scope.xmclass = null;
      }), 3000);
    });
    $location.url('/');
  })
  .controller('ViewContactCtrl', function($scope, $http, $location, $stateParams, $state) {
    $http.get('/api/contact/' + $stateParams.id)
    .success(function(data) {
      $scope.contact = data.contact;
    });
  })
  .controller('EditContactCtrl', function($scope, $http, $location, $stateParams) {
    $scope.phoneNumbr = /^\d{4}[-]?\d{7}$/; 
    $scope.form = {};  

    $http.get('/api/contact/' + $stateParams.id)
    .success(function(data) {
      $scope.form = data.contact;
    });

    $scope.editContact = function() {
      if($scope.updateForm.number.$error.pattern){
        return;
      }
      $http.put('/api/contact/' + $stateParams.id,$scope.form)
      .success(function(data) {
        $('#phonebookModal').modal('hide');
        $scope.$emit('updateContact', {
          message: 'Contact Updated.',
          xmclass: 'alert alert-success'
        });
      });
    };
    $scope.cancel = function() {
      $location.url('/');
    };
  })
  .controller('AddContactCtrl', function($scope, $http, $location) {
    $scope.phoneNumbr = /^\d{4}[- ]?\d{7}$/; 
    $scope.form = {};

    $scope.submitContact = function() {
        if($scope.addForm.number.$error.pattern){
          return;
        } 
        $http.post('/api/contact', $scope.form)
          .success(function(data) {
            $scope.contact = null;
            $('#phonebookModal').modal('hide');
            $location.url('/');
            $scope.$emit('updateContact', {
              message: 'Contact added.',
              xmclass: 'alert alert-success'
            });
            console.log($scope.form);
            $scope.form = {};
        });

    };
    $scope.cancel = function() {
      $location.url('/');
    };
  })
  .controller('DeleteContactCtrl', function($scope, $http, $location, $stateParams) {
    $scope.contact = {}
    $http.get('/api/contact/' + $stateParams.id)
      .then(function(res) {
        $scope.contact = res.data.contact;
      });
    $scope.deleteContact = function () {
      $http.delete('/api/contact/' + $stateParams.id)
      .then(function(data) {
        $('#phonebookModal').modal('hide');
        $scope.$emit('updateContact', {
          message: 'Contact deleted!',
          xmclass: 'alert bg-danger'
        });
        $location.url('/');
      });
    };
    $scope.cancel = function() {
      $location.url('/');
    };
  })
  .directive('formatPhone', [
    function() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, elem, attrs, ctrl, ngModel) {
                elem.add(phonenumber).on('keyup', function() {
                  elem.val(elem.val().replace(/[^0-9\-]/g,''));
                  var origVal = elem.val();
                   if(origVal.length === 4 && !origVal.match(/^\d{4}[- ]?\d{7}$/)) {
                     var phone = origVal.substring(0, 4) +'-';
                     elem.val(phone);
                   }
                });
            }
        };
      }
    ]);

})();
